;
jQuery(function ($) {


	function equalHeight(group, groupSize) {
		if (!group.length) {
			return;
		}
		groupSize = +(groupSize || 0);
		if (groupSize < 1) {
			groupSize = group.length;
		}
		var start = -groupSize, part;
		while ((part = group.slice(start += groupSize, start + groupSize)) && part.length) {
			part.outerHeight(Math.max.apply(null, $.makeArray(part.map(function () {
				return $(this).outerHeight();
			}))));
		}
	};

	$(document).ready(function () {
		//$('.side-nav .has-list > a').on('click', function (e) {
		//    e.preventDefault();
		//    var el = $(this).closest('li');
		//    el.find('>ul').stop().slideToggle();
		//    el.toggleClass('opened');
		//});


		//$('.side-nav > ul > li.opened ul').css('display', 'block');

		$('.btn-menu').on('click', function (e) {
			e.preventDefault();
			$('.main-nav').addClass('opened');
			$('.main-wrapper').addClass('menu-opened');
		});

		$('.btn-close-menu').on('click', function (e) {
			e.preventDefault();
			$('.main-nav').removeClass('opened');
			$('.main-wrapper').removeClass('menu-opened');
		});

		$('.tabs__nav').each(function () {
			$('.tab').not(':first').hide();

			$('li', this).removeClass('active');
			$('li:first-child', this).addClass('active');
			$('.tab:first-child').show();

			$('li', this).click(function (e) {
				e.preventDefault();

				var panel = $('a', this).attr('href');

				$(this).siblings().removeClass('active');
				$(this).addClass('active');
				$(panel).siblings().hide();
				$(panel).fadeIn(400);
			});
		});

		$('.main-nav .has-list > a').on('click', function (e) {
			e.preventDefault();

			var $this = $(this);

			$this.closest('ul').removeClass('current-menu').addClass('prev-menu');
			$this.closest('li').find('>.sub-menu').addClass('current-menu');
		});

		$('.main-nav .level-back a').on('click', function (e) {
			e.preventDefault();

			var $this = $(this);

			$this.closest('.sub-menu').removeClass('current-menu');
			$this.closest('.prev-menu').removeClass('prev-menu').addClass('current-menu');
		});


		$('.btn-mobile-search').on('click', function (e) {
			e.preventDefault();

			$('.header').toggleClass('active-mobile-search');
		});
	});


	function tapListener() {
		var screenSizeListener = true;
		var isTap = false;

		$(document)
			.on('click', function (e) {
				if (!$(e.target).closest('.main-nav').length && !$(e.target).closest('.btn-menu').length) {
					$('.main-wrapper').removeClass('menu-opened');
					$('.main-nav').removeClass('opened');
				}
			})
			.on('touchstart', '.footer__site__main__list .title', function () {
				isTap = true;

			}).on('touchmove', '.footer__site__main__list .title', function () {
				isTap = false;
			})
			.on('click','.btn-back-to-top',function(e){
				e.preventDefault();
				var src = $(this).attr('href');
				$('body, html').animate({scrollTop: $(src).offset().top + 'px'}, 'normal');
			})
		;

		$('.footer__site__main__list .title').on('touchend, click ', function (e) {

			if (e.type === 'click') {
				isTap = true;
			}
			if (isTap && $(window).width() <= 767) {
				$(this).closest('.footer__site__main__col').toggleClass('opened-list');
				$(this).closest('.footer__site__main__col').find('ul').stop().slideToggle();
			}
		});
	}

	tapListener();

	$(window).on('resize', function () {

		if ($(window).width() >= 786) {
			$('.footer__site__main__col').removeClass('opened-list');
			$('.footer__site__main__col ul').removeAttr('style');
		}

		$('[data-img-desk]').each(function () {
			var $this = $(this);
			if ($(window).width() <= 767) {
				$this.attr('src', $this.attr('data-img-device'));
			} else {
				$this.attr('src', $this.attr('data-img-desk'));
			}
		});

		setTimeout(function() {
			$('.honour-roll-grid .honour-item').height('auto');
			equalHeight($('.honour-roll-grid .honour-item'));
		},100);

	}).resize();

	if ($('.responsive-table').length) {
		$('.responsive-table').each(function () {
			var $this = $(this);
			var cols = $this.find('thead th');
			var rows = $this.find('tbody tr');
			var deviceTable = $('<div class="device-table"></div>');
			$this.after(deviceTable);
			var content, newRow, parameter;
			for (var c = 1; c < cols.length; c++) {
				deviceTable.append('<div class="row"><div class="title">' + cols.eq(c).html() + '</div>');
				newRow = $('.device-table .row').eq(c - 1);
				for (var r = 0; r < rows.length; r++) {
					content = rows.eq(c).find('td').eq(c).html();
					parameter = rows.eq(r).find('td').eq(0).html();
					newRow.append(parameter + content);
				}
			}
		});
	}

	$(window).on('load', function () {
		$('body').addClass('w-load');
		
		setTimeout(function() {
			$('.honour-roll-grid .honour-item').height('auto');
			equalHeight($('.honour-roll-grid .honour-item'));
		},100);
	});
});

