/**
 * Created by Eugene on 04.10.16.
 */
module.exports = function(grunt) {

    grunt.initConfig({
        sass: {
            options: {
                sourceMap: false
            },
            dist: {
                files: {
                    'assets/css/dev/vendors/font-awesome.css': ['assets/scss/font_awesome.scss'],
                    'assets/css/dev/guide.css': ['assets/scss/style_guide.scss'],
                    'assets/css/main.css': ['assets/scss/main.scss']
                }
            }
        },
        sprite:{
            all: {
                src: 'assets/images/sprite_images/*.png',
                dest: 'assets/images/sprite.png',
                destCss: 'assets/scss/_sprites.scss'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'assets/css/guide.min.css': ['assets/css/guide.css'],
                    'assets/css/main.min.css': ['assets/css/main.css']
                }
            }
        },
        concat_css: {
            all: {
                src: [
                    "assets/css/dev/**/*.css",//dev - temporary compilation directori
                    "assets/css/dev/*.css"//dev - temporary compilation directori
                ],
                dest: "assets/css/guide.css"
            }
        },
        concat: {
            dist: {
                src: [
                    'assets/js/lib/**/*.js',
                    'assets/js/lib/*.js'
                ],
                dest: 'assets/js/main.js'
            }
        },
        uglify: {
            options: {
                compress: {
                    global_defs: {
                        'DEBUG': false
                    },
                    dead_code: true
                }
            },
            my_target: {
                files: {
                    'assets/js/main.min.js': [
                        'assets/js/lib/**/*.js',
                        'assets/js/lib/*.js'
                    ]
                }
            }
        },
        clean: [
            "assets/css/dev"
        ],
        watch: {
            css:{
                files: [
                    'assets/scss/**/*.scss',
                    'assets/scss/*.scss'
                ],
                tasks: [
                    'sass',
                    'concat_css',
                    'clean'
                ],
                options: {
                    livereload: true
                }
            },
            scripts:{
                files: [
                    'assets/js/lib/*.js',
                    'assets/js/vendors/*.js'
                ],
                tasks: [
                    'concat',
                    'clean'
                ],
                options: {
                    interrupt: true
                }
            },
            configFiles:{
                files: ['Gruntfile.js'],
                option:{
                    reload:true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-spritesmith');

    grunt.registerTask('default', [ 'sprite', 'sass', 'concat_css', 'concat', 'clean', 'watch']);
    grunt.registerTask('minify', [ 'sprite', 'sass', 'concat_css','cssmin', 'uglify', 'clean']);
};
